# IAAgentsManager

Allow management of multiple AI agents in a single process. 
Configure agents contexts and parameters, define the global context and lets try agents interactions

## install

```bash
npm install
```

## configuration

make a copy of .env_template and rename it .env


```bash
cp .env_template .env
```
Go to openai.com and create an account if yoy don't have one. 
Then create an API key and paste it in the .env file

## develop

```bash
npm run dev
```


