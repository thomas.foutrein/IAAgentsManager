import { ChatgptService } from './services/ai/chatGPT.js';
let chatgptService = new ChatgptService();
let input = [
    { role: "system", content: "Tu es une personne qui s'appelle Gérard Bouchard" },
    { role: "user", content: "Bonjour comment t'appelles tu ?" },
    {
        role: "assistant",
        content: "Je m'appelle Gérard Bouchard",
    },
    { role: "user", content: "Du coup quel est ton nom de famille ?" },
];
let response = await chatgptService.askWithContextandChatHistory(input);
console.log(response);
console.log(`Used tokens in this session : ${chatgptService.totalUsedTokens}`);
