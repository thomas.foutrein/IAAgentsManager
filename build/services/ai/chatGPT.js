// chatgpt service using openai api
import { Configuration, OpenAIApi } from "openai";
export var Model;
(function (Model) {
    Model["GPT4"] = "gpt-4-0314";
    Model["GPT4_32k"] = "gpt-4-32k\t";
    Model["GPT3_5"] = "gpt-3.5-turbo";
})(Model || (Model = {}));
export class ChatgptService {
    openai;
    totalUsedTokens = 0;
    simulate = true;
    constructor() {
        const configuration = new Configuration({
            apiKey: process.env.OPENAI_API_KEY,
        });
        this.openai = new OpenAIApi(configuration);
        this.simulate = process.env.SIMULATE_CHATGPT === "true" ? true : false;
        this.simulate ? console.log("### ChatGPT is in simulation mode ###") : console.log("### ChatGPT is in production mode ###");
    }
    async askWithContextandChatHistory(input, model = Model.GPT3_5) {
        if (this.simulate) {
            return {
                data: { choices: [{ message: { content: "I am a chatbot" } }] },
            };
        }
        else {
            try {
                const response = await this.openai.createChatCompletion({
                    model: model,
                    messages: input,
                });
                console.log(`--- RESPONSE FROM ChatGPT ---\n ${response.data.choices[0].message?.content}`);
                this.totalUsedTokens += response.data.usage?.total_tokens || 0;
                return response.data;
            }
            catch (error) {
                console.log(`Error ${error}`);
                return error;
            }
        }
    }
}
