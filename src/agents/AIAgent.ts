import { ChatgptService } from '../services/ai/chatGPT.ts';

class AIAgent {
    public agentAttributes: AgentAttributes;
    private chatGPTService: ChatgptService;

    constructor(agentAttributes: AgentAttributes, chatGPTService: ChatgptService) {
        this.agentAttributes = agentAttributes;
        this.chatGPTService = chatGPTService;
    }

    async buildAgent() {
        //TODO: build the agent by calling the chatGPTService
    }
}

export interface AgentAttributes {
    firstname: string;
    lastname: string;
    age: number; // in years
    size: number; // in cm
    weight: number; // in kg
    job: string; // e.g. "doctor", "policeman", "teacher", "student", "unemployed", "retired", "other"
}